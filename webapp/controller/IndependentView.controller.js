sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.IndependentView", {
		onLeaveTypeChange: function(oEvent) {
			//處理Model相關資訊，並執行publish
			//var oModel = new sap.ui.model.json.JSONModel("model/Sample.json");
			//sap.ui.getCore().setModel(oModel, "user");
			var sText = oEvent.getParameter("selectedItem").getProperty("text");
			var oModel = this.getView().getModel("user");
			oModel.setProperty("/LeaveType", sText);
			this.getView().setModel(oModel, "user");
			// console.log(sText);
			// console.log(oModel);
			this.publishDataToHost(oModel);
		},
		publishDataToHost: function(oData) {
			sap.ui.getCore().getEventBus().publish("Flow7", "FORM_DATA_CHANGE", oData);
		}
	});

});