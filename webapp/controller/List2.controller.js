sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"nnext/iq/Processing/model/Formatter",
		"sap/ui/core/routing/History"
], function(Controller, Formatter,History) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.List2", {
		oFormatter: Formatter,

		onInit: function() {
			var ctrl = this;

			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);

			$.ajax("/Flow7Api_CLONE/api/dashboard/processing")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);

					ctrl.getView().setModel(oData, "requisitions");
				});
		},
		onPress: function(oEvent) {
				this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);
				this._oRouter.navTo("FormContent" );
				console.log(oEvent.getSource().getBindingContext("requisitions"));
			}
			/**
			 * Called when a controller is instantiated and its View controls (if available) are already created.
			 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
			 * @memberOf nnext.iq.Processing.view.List2
			 */
			//	onInit: function() {
			//
			//	},

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf nnext.iq.Processing.view.List2
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf nnext.iq.Processing.view.List2
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf nnext.iq.Processing.view.List2
		 */
		//	onExit: function() {
		//
		//	}

	});

});