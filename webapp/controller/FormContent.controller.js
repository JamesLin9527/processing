sap.ui.define([
	"sap/ui/core/mvc/Controller"
], function(Controller) {
	"use strict";

	return Controller.extend("nnext.iq.Processing.controller.FormContent", {
		onInit: function() {
			var ctrl = this;

			this._oRouter = sap.ui.core.UIComponent.getRouterFor(this);

			$.ajax("/Flow7Api_CLONING/api/fdp/m/456779fd-e5d0-4a39-bfa2-0a58f6c7ad76")
				.done(function(data) {
					var oData = new sap.ui.model.json.JSONModel(data);
					//console.log(oData);
					ctrl.getView().setModel(oData, "ContentData");
				});

		}
	});
});